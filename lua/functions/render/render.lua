local node = require('functions.render.node')

-- Define the size and position of the floating window
local width = 40
local height = 10
local row = math.floor((vim.o.lines - height) / 2)  -- Center vertically
local col = math.floor((vim.o.columns - width) / 2) -- Center horizontally

-- Options for the floating window
local windowOpts = {
    relative = 'editor', -- Position relative to the entire editor
    width = width/2,
    height = height/2,
    col = col,
    row = row,
    anchor = 'NW',   -- Window anchor is the top-left corner
    style = 'minimal', -- Minimal style: no number, no sign column, etc.
    border = 'rounded' -- You can use 'single', 'double', 'rounded', 'solid', or 'shadow'
}

-- Create a buffer for the floating window
local buf = vim.api.nvim_create_buf(false, true) -- false = not listed, true = scratch

-- Create a horizontal split and return the window ID
local function create_hsplit(buffer)
  -- If no buffer is provided, create a new one
  buffer = buffer or vim.api.nvim_create_buf(true, false)
  
  -- Create horizontal split
  vim.cmd('split')
  
  -- Get the window ID of the newly created window
  local win_id = vim.api.nvim_get_current_win()
  
  -- Set the buffer in the new window
  vim.api.nvim_win_set_buf(win_id, buffer)
  
  return win_id
end

local function app()
    return node.NewVBox(
        node.NewText('Test App'),
        node.NewHBox(
            node.NewText('1'),
            node.NewVBox(
                node.NewText('short'),
                node.NewHBox(
                    node.NewText('nested Hbox 1'),
                    node.NewText('nested Hbox 200000000000000000000000000000000000000000000000000000000000000000000000')
                )
            ),
            node.NewText('3')
        )
    )
end

---@param rootNode Node[]
local function render(rootNode)
    local lines = {}

    ---@param value string
    local function insert(value)
        table.insert(lines, value)
    end

    ---@param parent Node
    local function traverse(parent)
        for _, child in ipairs(parent.children) do
            if type(child.render) == 'function' then
                for _, node in ipairs(child:render()) do
                    table.insert(lines, node)
                end
            end
        end
    end

    traverse(rootNode)


    vim.api.nvim_buf_set_lines(buf, 0, -1, false, lines)
    vim.print('output lines')

    vim.print(lines)

    create_hsplit(buf)



    -- Create the floating window with the buffer
    -- local win = vim.api.nvim_open_win(buf, true, windowOpts)

end

render(app())
