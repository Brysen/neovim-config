local M = {}

---Base class for the node structure
---@class Node
---@field type string
---@field children Node[]
---@field appendChild fun(child: Node)

---Create a base node
---@return Node
local function newNode()
    local node = {}
    node.children = {}
    node.type = 'base'

    ---Add a child to the node
    ---@param child Node
    function node:appendChild(child)
        table.insert(self.children, child)
    end

    return node
end

---Node for basic text input
---@class TextNode: Node
---@field content string
---@field render fun(props: any): string

---@param content string
---@return TextNode
local function newTextNode(content)
    ---@class TextNode
    local node = newNode()
    node.content = content
    node.type = 'text'

    function node:render()
        return {self.content}
    end

    return node
end

local function mergeTablesHorizontally(...)
    local tables = {...}
    if #tables == 0 then return {} end

    -- Find the maximum height (number of elements in the longest table)
    local maxHeight = 0
    for _, t in ipairs(tables) do
        maxHeight = math.max(maxHeight, #t)
    end

    -- Find the maximum width for each table
    local maxWidths = {}
    for i, t in ipairs(tables) do
        maxWidths[i] = 0
        for _, str in ipairs(t) do
            maxWidths[i] = math.max(maxWidths[i], #str)
        end
    end

    -- Create the merged result
    local result = {}
    for row = 1, maxHeight do
        local mergedRow = ""
        for col, t in ipairs(tables) do
            local str = t[row] or ""  -- Use empty string if no value exists
            -- Pad the string with spaces to match the maximum width
            mergedRow = mergedRow .. str .. string.rep(" ", maxWidths[col] - #str)
        end
        table.insert(result, mergedRow)
    end

    return result
end

---Node for horizontal alignment
---@class HBox: Node
---@field render fun(props: any): string[]

---@param ... Node[]
---@return HBox
local function newHBox(...)
    ---@class HBox
    local node = newNode()
    node.type = 'hbox'

    for _, value in ipairs({...}) do
        table.insert(node.children, value)
    end

    function node:render()
        ---@type string[]
        local buffer = {}

        for _, child in ipairs(self.children) do
            if type(child.render) == 'function' then
                local output = child:render()

                buffer = mergeTablesHorizontally( buffer, output )
            end
        end

        return buffer
    end

    return node
end

---Node for horizontal alignment
---@class VBox: Node
---@field render fun(props: any): string[]


---@param ... Node[]
---@return VBox
local function newVBox(...)
    ---@class VBox
    local node = newNode()
    node.type = 'vbox'

    for _, child in ipairs({...}) do
        table.insert(node.children, child)
    end

    function node:render()
        ---@type string[]
        local buffer = {}

        for _, child in ipairs(self.children) do
            if type(child.render) == 'function' then
                for _, node in ipairs(child:render()) do
                    table.insert(buffer, node)
                end
            end
        end

        return buffer
    end

    return node
end

M.NewNode = newNode
M.NewText = newTextNode
M.NewHBox = newHBox
M.NewVBox = newVBox
M.foo = 2

return M
