function LittleWindow()
  -- Define the content to display in the floating window
  local lines = { " Run " .. " Task Dev" }

  -- Create a buffer for the floating window
  local buf = vim.api.nvim_create_buf(false, true) -- false = not listed, true = scratch

  -- Set the content of the buffer
  vim.api.nvim_buf_set_lines(buf, 0, -1, false, lines)

  -- Make the buffer read-only by setting the 'modifiable' option to false
  vim.api.nvim_buf_set_option(buf, 'modifiable', false)

  -- Define the size and position of the floating window
  local width = 40
  local height = 10
  local row = math.floor((vim.o.lines - height) / 2) -- Center vertically
  local col = math.floor((vim.o.columns - width) / 2) -- Center horizontally

  -- Options for the floating window
  local opts = {
    relative = 'editor', -- Position relative to the entire editor
    width = width,
    height = height,
    col = col,
    row = row,
    anchor = 'NW',   -- Window anchor is the top-left corner
    style = 'minimal', -- Minimal style: no number, no sign column, etc.
    border = 'rounded' -- You can use 'single', 'double', 'rounded', 'solid', or 'shadow'
  }

  -- Create the floating window with the buffer
  local win = vim.api.nvim_open_win(buf, true, opts)

  -- Define a custom highlight group
  vim.api.nvim_set_hl(0, "MyWindowText", { bg = "#ff9e64", fg = "#282c34", bold = true })

  -- Apply the highlight group to the lines in the buffer
  vim.api.nvim_buf_add_highlight(buf, -1, "MyWindowText", 0, 0, 5) -- Entire first line

  -- Function to close the window
  local function closeWindow()
    vim.api.nvim_win_close(0, true) -- Close the current window
  end

  local function handleClick()
    local cursor = vim.api.nvim_win_get_cursor(0)

    if cursor[2] >= 0 and cursor[2] <= 4 and cursor[1] == 1 then
      print('button clicked')
    end
    
  end

  -- Set a keymap to close the window with 'q'
  vim.keymap.set('n', 'q', closeWindow, { noremap = true, silent = true, buffer = buf })
  vim.keymap.set('n', '<CR>', handleClick, { noremap = true, silent = true, buffer = buf})
end
