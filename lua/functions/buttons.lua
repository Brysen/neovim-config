local function create_button_window()
    -- Create a new buffer
    local buf = vim.api.nvim_create_buf(false, true)
    
    -- Button definitions
    local buttons = {
        { label = "Save File", action = "w" },
        { label = "Quit", action = "q" },
        { label = "Save & Quit", action = "wq" },
        { label = "File Explorer", action = "Ex" }
    }
    
    -- Calculate window size and position
    local width = 30
    local height = #buttons + 2
    local win_opts = {
        relative = "editor",
        width = width,
        height = height,
        row = math.floor((vim.o.lines - height) / 2),
        col = math.floor((vim.o.columns - width) / 2),
        style = "minimal",
        border = "rounded"
    }
    
    -- Create the window
    local win = vim.api.nvim_open_win(buf, true, win_opts)
    
    -- Prepare buffer content
    local content = {}
    local keymaps = {}
    
    -- Add padding line
    table.insert(content, "")
    
    -- Add buttons with centered text
    for i, button in ipairs(buttons) do
        local padding = math.floor((width - #button.label - 2) / 2)
        local line = string.rep(" ", padding) .. button.label
        table.insert(content, line)
        
        -- Store keymap data
        keymaps[i] = {
            key = tostring(i),
            action = button.action
        }
    end
    
    -- Set buffer content
    vim.api.nvim_buf_set_lines(buf, 0, -1, false, content)
    
    -- Set buffer options
    vim.api.nvim_buf_set_option(buf, 'modifiable', false)
    vim.api.nvim_buf_set_option(buf, 'buftype', 'nofile')
    
    -- Set keymaps
    for _, keymap in ipairs(keymaps) do
        vim.api.nvim_buf_set_keymap(buf, 'n', keymap.key, 
            string.format([[<cmd>%s<CR>]], keymap.action),
            { noremap = true, silent = true })
    end
    
    -- Add keymap to close window
    vim.api.nvim_buf_set_keymap(buf, 'n', 'q', 
        '<cmd>close<CR>',
        { noremap = true, silent = true })
    
    -- Add highlighting
    vim.api.nvim_win_set_option(win, 'winhl', 'Normal:PopupNormal')
    
    return buf, win
end

-- Command to show the button window
vim.api.nvim_create_user_command('ShowButtons', create_button_window, {})
