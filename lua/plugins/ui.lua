return {
    {
        "folke/which-key.nvim",
        event = "VeryLazy",
        init = function()
            vim.o.timeout = true
            vim.o.timeoutlen = 300
        end,
        opts = {
            -- your configuration comes here
            -- or leave it empty to use the default settings
            -- refer to the configuration section below
        }
    },
--    {
--        "miversen33/sunglasses.nvim",
--        config = true,
--        event = "UIEnter",
--        opts = {
--            filter_type = "SHADE",
--            filter_percent = .65
--        },
--        keys = {
--            { '<leader>S', '<cmd>SunglassesEnableToggle<cr>', desc = 'Toggle Window Dimming (sunglasses)' },
--        },
--    },
    {
        'nvim-lualine/lualine.nvim',
        dependencies = { 'nvim-tree/nvim-web-devicons' },
        opts = {
            options = {
                theme = require('lualineTheme').theme(),
                section_separators = { left = ' ', right = '' },
                component_separators = ' ',
            },
        }
    },
    {
        'projekt0n/github-nvim-theme',
        name = 'github-theme',
        lazy = false,    -- make sure we load this during startup if it is your main colorscheme
        priority = 1000, -- make sure to load this before all the other start plugins
        config = function()
            require('github-theme').setup({
                -- ...
            })

            vim.cmd('colorscheme github_dark_default')

            --vim.api.nvim_get_hl()


            vim.api.nvim_set_hl(0, 'Normal', { bg = None, ctermbg = None })
            vim.api.nvim_set_hl(0, 'NonText', { bg = None, ctermbg = None })
            vim.api.nvim_set_hl(0, 'NormalNC', { bg = None })
            vim.api.nvim_set_hl(0, 'NvimTreeNormal', { bg = None })
            vim.api.nvim_set_hl(0, 'LineNr', { fg = "white" })
            vim.api.nvim_set_hl(0, 'LineNrAbove', { fg = "#C3A8ED" })
            vim.api.nvim_set_hl(0, 'LineNrBelow', { fg = "#C3A8ED" })
            vim.api.nvim_set_hl(0, 'CursorLineNr', { fg = "#ff0000" })

            local transState = true
            function TransToggle()
                if transState then

                end
            end

            --vim.api.nvim_create_user_command('TransToggle', )
        end,
    },
}
