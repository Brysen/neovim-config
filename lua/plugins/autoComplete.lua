return {
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        config = function()
            local configs = require("nvim-treesitter.configs")

            configs.setup({
                ensure_installed = {
                    "c",
                    "go",
                    "typescript",
                    "lua",
                    "vim",
                    "vimdoc",
                    "tsx",
                    "query",
                    "javascript",
                    "html",
                    "php",
                },
                sync_install = false,
                highlight = { enable = true },
                indent = { enable = true },
            })
        end,
    },
    {
        'brenoprata10/nvim-highlight-colors',
        init = function()
            require("nvim-highlight-colors").setup {
                ---Render style
                ---@usage 'background'|'foreground'|'virtual'
                render = 'background',

                ---Set virtual symbol (requires render to be set to 'virtual')
                virtual_symbol = '■',

                ---Set virtual symbol suffix (defaults to '')
                virtual_symbol_prefix = '',

                ---Set virtual symbol suffix (defaults to ' ')
                virtual_symbol_suffix = ' ',

                ---Set virtual symbol position()
                ---@usage 'inline'|'eol'|'eow'
                ---inline mimics VS Code style
                ---eol stands for `end of column` - Recommended to set `virtual_symbol_suffix = ''` when used.
                ---eow stands for `end of word` - Recommended to set `virtual_symbol_prefix = ' ' and virtual_symbol_suffix = ''` when used.
                virtual_symbol_position = 'inline',

                ---Highlight hex colors, e.g. '#FFFFFF'
                enable_hex = true,

                ---Highlight short hex colors e.g. '#fff'
                enable_short_hex = true,

                ---Highlight rgb colors, e.g. 'rgb(0 0 0)'
                enable_rgb = true,

                ---Highlight hsl colors, e.g. 'hsl(150deg 30% 40%)'
                enable_hsl = true,

                ---Highlight CSS variables, e.g. 'var(--testing-color)'
                enable_var_usage = true,

                ---Highlight named colors, e.g. 'green'
                enable_named_colors = true,

                ---Highlight tailwind colors, e.g. 'bg-blue-500'
                enable_tailwind = false,


                -- Exclude filetypes or buftypes from highlighting e.g. 'exclude_buftypes = {'text'}'
                exclude_filetypes = {},
                exclude_buftypes = {}
            }
        end
    },
    {
        'hrsh7th/nvim-cmp',
        init = function()
            local cmp = require('cmp')
            local lsp_zero = require('lsp-zero')
            local cmp_select = { behavior = cmp.SelectBehavior.Select }

            local formatting = lsp_zero.cmp_format()
            formatting.format = function(entry, item)
                local color_item = require("nvim-highlight-colors").format(entry, { kind = item.kind })
                item = lsp_zero.cmp_format({
                    -- any lspkind format settings here
                }).format(entry, item)
                if color_item.abbr_hl_group then
                    item.kind_hl_group = color_item.abbr_hl_group
                    item.kind = color_item.abbr
                end
                return item
            end

            cmp.setup({
                sources = {
                    { name = 'path' },
                    { name = 'nvim_lsp' },
                    { name = 'nvim_lua' },
                },
                formatting = formatting,
                -- formatting = {
                --     format = function(entry, item)
                --         local color_item = require("nvim-highlight-colors").format(entry, { kind = item.kind })
                --         item = lsp_zero.cmp_format({
                --             -- any lspkind format settings here
                --         })(entry, item)
                --         if color_item.abbr_hl_group then
                --             item.kind_hl_group = color_item.abbr_hl_group
                --             item.kind = color_item.abbr
                --         end
                --         return item
                --     end
                -- },
                mapping = cmp.mapping.preset.insert({
                    ['<S-tab>'] = cmp.mapping.select_prev_item(cmp_select),
                    ['<tab>'] = cmp.mapping.select_next_item(cmp_select),
                    ['<cr>'] = cmp.mapping.confirm({ select = true }),
                    ['<C-Space>'] = cmp.mapping.complete(),
                }),
            })
        end
    },
    { 'onsails/lspkind.nvim' },
    {
        'windwp/nvim-ts-autotag',
        init = function()
            require('nvim-ts-autotag').setup({
                opts = {
                    -- Defaults
                    enable_close = true,          -- Auto close tags
                    enable_rename = true,         -- Auto rename pairs of tags
                    enable_close_on_slash = true -- Auto close on trailing </
                },
            })
        end
    },
    { 'L3MON4D3/LuaSnip' },
}
