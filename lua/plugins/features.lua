return {
  {
    'nvim-tree/nvim-tree.lua',
    opts = function()
      -- empty setup using defaults
      require("nvim-tree").setup()
    end

  },
  {
    "nvim-telescope/telescope-file-browser.nvim",
    dependencies = { "nvim-telescope/telescope.nvim", "nvim-lua/plenary.nvim" }
  },
  {
    "mbbill/undotree",
    keys = {
      { '<leader>u', '<cmd>UndotreeToggle<cr>', desc = 'Undotree' },
    }
  },
  {
    "tpope/vim-fugitive",
    keys = {
      { '<leader>gs', '<cmd>Git<cr>', desc = 'Vim Fu*GIT*ive' },
    }
  },
  {
    '2kabhishek/nerdy.nvim',
    dependencies = {
      'stevearc/dressing.nvim',
      'nvim-telescope/telescope.nvim',
    },
    cmd = 'Nerdy',
  },

  {
    "rest-nvim/rest.nvim",
    dependencies = {
      "nvim-treesitter/nvim-treesitter",
      opts = function(_, opts)
        opts.ensure_installed = opts.ensure_installed or {}
        table.insert(opts.ensure_installed, "http")
      end,
    }
  }
  --    {
  --        'nvimdev/dashboard-nvim',
  --        event = 'VimEnter',
  --        config = function()
  --            require('dashboard').setup {
  --                -- config
  --            }
  --        end,
  --        dependencies = { { 'nvim-tree/nvim-web-devicons' } }
  --    },
}
