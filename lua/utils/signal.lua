local M = {}

---@class Signal
---@field getValue fun(): any
---@field setValue fun(newValue: any)
---@field subscribe fun(callback: fun(value: any))

---@param value any
---@return Signal
function M.New(value)
    local internalValue = value

    local subscribers = {}

    local function emit()
        for _, subscriber in ipairs(subscribers) do
            subscriber(internalValue)
        end
    end

    local signal = {
        ---
        ---@return any
        getValue = function ()
            return internalValue
        end,
        ---
        ---@param newValue any
        setValue = function (newValue)
            internalValue = newValue
            emit()
        end,
        ---
        ---@param callback any
        subscribe = function (callback)
            table.insert(subscribers, callback)
        end,
    }
    return signal
end

return M
