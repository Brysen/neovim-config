vim.g.neovide_transparency = 0.8
vim.g.transparency = 0.8

vim.g.neovide_window_blurred = true

vim.g.neovide_floating_blur_amount_x = 2.0
vim.g.neovide_floating_blur_amount_y = 2.0

vim.g.neovide_cursor_vfx_mode = "pixiedust"

vim.o.guifont = "JetBrainsMono Nerd Font:h14"

vim.keymap.set('v', '<D-c>', '"+y')           -- Copy
vim.keymap.set('n', '<D-v>', '"+P')           -- Paste normal mode
vim.keymap.set('v', '<D-v>', '"+P')           -- Paste visual mode
vim.keymap.set('c', '<D-v>', '<C-R>+')        -- Paste command mode
vim.keymap.set('i', '<D-v>', '<ESC>l"+Pli')   -- Paste insert mode
