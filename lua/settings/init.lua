require('functions.floatWindow')

if vim.g.neovide then
    require('settings.neovide')
end

-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- optionally enable 24-bit colour
vim.opt.termguicolors = true

-- Set tab width to 2 spaces
vim.o.tabstop = 2
vim.o.shiftwidth = 2
vim.o.expandtab = true

vim.o.number = true
vim.o.relativenumber = true

local cwd_init_path = vim.fn.getcwd() .. "/.neovim/init.lua"

-- Check if the local init.lua exists
if vim.fn.filereadable(cwd_init_path) == 1 then
    vim.cmd("luafile " .. cwd_init_path)
    print("Loaded local init.lua from " .. cwd_init_path)
else
    print("No local init.lua found in " .. vim.fn.getcwd())
end

