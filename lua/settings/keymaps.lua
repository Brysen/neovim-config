vim.g.mapleader = " "

local map = vim.keymap.set

-- Open Netrw in a vertical split
map('n', '<leader>e', ':NvimTreeToggle<CR>', { desc = "Open Netrw in a vertical split", noremap = true, silent = true })


-- Open a Termainl in a vertical or horizontal split
map('n', '<leader>tv', ':vsplit term://fish<cr>',
  { desc = "Open terminal in a vertical split", noremap = true, silent = true })
map('n', '<leader>th', ':split term://fish<cr>',
  { desc = "Open terminal in a horizontal split", noremap = true, silent = true })


-- better up/down
map({ "n", "x" }, "j", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
map({ "n", "x" }, "<Down>", "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
map({ "n", "x" }, "k", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
map({ "n", "x" }, "<Up>", "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })

-- Move to window using the <ctrl> hjkl keys
map("n", "<C-h>", "<C-w>h", { desc = "Go to left window", remap = true })
map("n", "<C-j>", "<C-w>j", { desc = "Go to lower window", remap = true })
map("n", "<C-k>", "<C-w>k", { desc = "Go to upper window", remap = true })
map("n", "<C-l>", "<C-w>l", { desc = "Go to right window", remap = true })

---@param desc string
---@param event unknown
local function createLspKeymapOpts(event, desc)
  return { buffer = event.buf, noremap = true, desc = 'LSP: ' .. desc, silent = true }
end


-- LSP key binds
vim.api.nvim_create_autocmd('LspAttach', {
  desc = 'LSP actions',
  callback = function(event)
    local opts = { buffer = event.buf }

    map('n', '<leader>li', ':lua vim.lsp.buf.hover()<CR>', createLspKeymapOpts(event, 'Hover Info'))
    map('n', '<leader>lr', ':lua vim.lsp.buf.rename()<CR>', createLspKeymapOpts(event, 'Rename'))
    map('n', '<leader>lf', ':lua vim.lsp.buf.format({async = true})<CR>', createLspKeymapOpts(event, 'Format Buffer'))

    map('n', 'K', '<cmd>lua vim.lsp.buf.hover()<cr>', createLspKeymapOpts(event, 'hover info'))
    map('n', '<leader>ld', '<cmd>lua vim.lsp.buf.definition()<cr>', createLspKeymapOpts(event, 'Definition'))
    map('n', '<leader>lD', '<cmd>lua vim.lsp.buf.declaration()<cr>', createLspKeymapOpts(event, 'Declaration'))
    map('n', '<leader>lI', '<cmd>lua vim.lsp.buf.implementation()<cr>', createLspKeymapOpts(event, 'Implementation'))
    map('n', '<leader>lo', '<cmd>lua vim.lsp.buf.type_definition()<cr>', createLspKeymapOpts(event, 'Type Definition'))
    map('n', '<leader>lR', '<cmd>lua vim.lsp.buf.references()<cr>', createLspKeymapOpts(event, 'References'))
    map('n', '<leader>ls', '<cmd>lua vim.lsp.buf.signature_help()<cr>', createLspKeymapOpts(event, 'Signature Help'))
    map('n', '<leader>la', '<cmd>lua vim.lsp.buf.code_action()<cr>', createLspKeymapOpts(event, 'Code Action'))
  end,
})
